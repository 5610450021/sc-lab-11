package part5;

import java.util.ArrayList;

public class Refrigerator {
	private int size ;
	ArrayList<String> things = new ArrayList<String>();

	public Refrigerator(int size){
		this.size = size;
	}
	
	public void put(String stuff) throws FullException{
		if (things.size() < size) {
			things.add(stuff);
		
		}
		else{
			throw new FullException();
		}
	}
	
	public String takeOut(String stuff){
		if (things.contains(stuff)) {
			things.remove(stuff);

			return stuff;
		}
		else{
			return null;
		}
	}
	
	public String toString(){
		String str = "";
		for (String string : things) {
			str += string + "\n";
		}
		return str;
	}
}
