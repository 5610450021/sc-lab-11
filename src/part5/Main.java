package part5;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Refrigerator refri = new Refrigerator(10);
		try {
			refri.put("Apple");
			refri.put("Banana");
			refri.put("Melon");
			refri.put("Chocolate");
			refri.put("Orange");
			refri.put("Water");
			refri.put("Strawberry");
			refri.put("Mango");
			refri.put("Ice");
			refri.put("Icecream");
			System.out.println("Food in Refrigerator:");
			System.out.println(refri.toString());
			System.out.println("--------------------------------------------------");
			System.out.println("TakeOut Food from Refrigerator:");
			System.out.println(refri.takeOut("Apple"));
			refri.put("Yogurt");
			System.out.println("--------------------------------------------------");
			System.out.println("Food in Refrigerator:");
			System.out.println(refri.toString());
			refri.put("Pork");
			System.out.println(refri.toString());
		} catch (FullException e) {
			// TODO Auto-generated catch block
			System.err.println("Refrigerator is full!.You can't put food in it.");
		}
	}

}
