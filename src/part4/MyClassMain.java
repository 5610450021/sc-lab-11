package part4;

public class MyClassMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		} catch (DataException e){
			System.out.print("D");
		} catch (FormatException e){
			System.out.print("E");
		} finally {
			System.out.print("F");
		}
		System.out.print("G");
	}

}
