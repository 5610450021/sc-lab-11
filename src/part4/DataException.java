package part4;

public class DataException extends Exception {
	public DataException(){}
	public DataException(String message){
		super(message);
	}
}
