package part4;

public class MyClass {
	public void methX() throws DataException{
		//.....code.....
		throw new DataException();
	}

	public void methY(){
		//.....code.....
		throw new FormatException("Error");
	}
}

/** 1.At method methX(),add "throws DataException" at line4 and add "new" between throw and DataException();
	2.ABCFG
	3.ADFG
	4.AEFG
**/