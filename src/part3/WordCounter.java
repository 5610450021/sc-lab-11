package part3;

import java.util.HashMap;

public class WordCounter {

	String message;
	HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
	}
	
	public void count(){
		wordCount = new HashMap<String, Integer>();
		String[] liststr = message.split(" ");
		int count = 0;
		for (String str : liststr) {
			if (wordCount.containsKey(str)) {
				wordCount.put(str,++count);
			} else {
				wordCount.put(str, 1);
			}
		}
	}
	
	public int hasWord(String word){
		if (wordCount.containsKey(word)) {
			return wordCount.get(word);
		}
		return 0;
	}
}
